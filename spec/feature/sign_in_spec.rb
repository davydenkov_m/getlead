# -*- encoding : utf-8 -*-
require_relative '../spec_helper'

feature 'Shall check devise authorization' do

  scenario "shall authorize User with correct email and password", js: true  do
    visit new_user_session_path
    user = User.find(1)
    fill_in "user[email]", with: user.email
    fill_in "user[password]", with: user.password
   # click_button "Войти"
    #page.execute_script("$('#sign_in_button').click()")
    find('input#sign_in_button.btn.btn-primary.btn-success').click()
    #find("//input[@class='btn btn-primary btn-success'][@id='sign_in_button']").click()
    has_selector? 'input#created_at_first'
  end

end
