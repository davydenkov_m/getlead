# -*- encoding : utf-8 -*-
require_relative '../spec_helper'

FactoryGirl.define do
  factory :user_admin, class: User do
    email "davydenkov.mihail@gmail.com"
    password "12345678"
  end
end