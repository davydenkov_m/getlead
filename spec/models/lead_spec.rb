# -*- encoding : utf-8 -*-
require_relative '../spec_helper'

describe Lead do

  let(:lead) { Lead.new(fio: "Иванов Иван Иванович", city: "Москва", phone: "9163671952", access_token: 'ffdsqwe123asewq1')}

  context 'VALIDATORS' do
    it 'invalid without required fields' do
      lead = Lead.new
      expect(lead).not_to be_valid
      expect(Lead._validators.keys - [:phone, :city, :fio, :access_token, :region]).to be_empty
    end

    it 'valid with fio, city and phone' do
      expect(lead).to be_valid
    end

    it 'is should validate phone number' do
      lead = Lead.new(fio: "Иванов Иван Иванович", city: "Москва", phone: "+2163671952", access_token: 'ffdsqwe123asewq1')
      expect(lead).not_to be_valid
      lead = Lead.new(fio: "Иванов Иван Иванович", city: "Москва", phone: "9163671952", access_token: 'ffdsqwe123asewq1')
      expect(lead).to be_valid
    end
  end

  context 'AR REFLECTIONS' do
    it 'shall belongs to user' do
      expect(Lead.reflections.include?(:user)).to be_true
      lead = Lead.create(fio: "Иванов Иван Иванович", city: "Москва", phone: "9163671952", access_token: 'ffdsqwe123asewq1', partner_source: 'admin')
      expect(lead.user).not_to be nil
    end
  end

  context 'CALLBACKS' do

    it 'shall call \'send_to_SPL\' after create if accepted status' do
      expect(ToSplWorker).to receive(:perform_async)
      lead = Lead.create(fio: "Иванов Иван Иванович", city: "Москва", phone: "9163671952", access_token: 'qweqwe1', partner_source: 'qweqwe', lead_status: "Прошла валидацию")
      lead.user.stub(:active?) { true }

      expect(ToSplWorker).not_to receive(:perform_async)
      lead = Lead.create(fio: "Иванов Иван Иванович", city: "Москва", phone: "9163671952", access_token: 'qweqwe1', partner_source: 'qweqwe', lead_status: "qweqweasd")
      lead.user.stub(:active?) { true }

      expect(ToSplWorker).not_to receive(:perform_async)
      lead = Lead.create(fio: "Иванов Иван Иванович", city: "Москва", phone: "9163671952", access_token: 'qweqwe1', partner_source: 'qweqwe', lead_status: "Прошла валидацию")
      lead.user.stub(:active?) { false }
    end

    it 'shall call \'send_to_SPL\' when called #save if record exist *.times' do
      lead = Lead.new(fio: "Тест тест тест", city: "Москва", phone: "9163671952", access_token: 'qweqwe1', partner_source: 'qweqwe', lead_status: "Прошла валидацию")
      expect(ToSplWorker).to receive(:perform_async)
      lead.user.stub(:active?) { true }
      lead.save

      lead = Lead.new(fio: "Тест тест тест", city: "Москва", phone: "9163671952", access_token: 'qweqwe1', partner_source: 'qweqwe', lead_status: "Прошла валидацию")
      expect(ToSplWorker).to receive(:perform_async)
      lead.user.stub(:active?) { true }
      lead.save
      lead.update(lead_status: 'qqq')

      #lead = Lead.new(fio: "Тест тест тест", city: "Москва", phone: "9163671952", access_token: 'qweqwe1', partner_source: 'qweqwe', lead_status: "Прошла валидацию")
      #expect(ToSplWorker).not_to receive(:perform_async)
      #lead.user.stub(:active?) { true }
      #lead.update(spl_status: "Принята (анкета заполнена)")

      lead = Lead.new(fio: "Тест тест тест", city: "Москва", phone: "9163671952", access_token: 'qweqwe1', partner_source: 'qweqwe', lead_status: "Прошла валидацию")
      expect(ToSplWorker).to receive(:perform_async)
      lead.user.stub(:active?) { true }
      lead.save
      expect(ToSplWorker).not_to receive(:perform_async)
      lead.update(lead_status: 'qqq')
    end

  end

  context 'PAYLOAD' do
    it 'shall return counters hash' do
      user = User.find(1)
      client_ids = User.pluck(:partner_source).uniq.compact
      counters = Lead.set_counters("2014-02-01", "2014-10-01", client_ids, user.admin?)
      expect(counters).to be_kind_of(Hash)
      expect(counters).not_to be_empty
    end
  end

  context 'SCOPES' do
    it 'shall find partners' do
      client_ids = []
      expect { Lead.find_partners(client_ids, admin=true)}.not_to raise_exception
    end

    it 'shall return lead_status' do
      status = "Прошла валидацию"
      expect { Lead.status(status) }.not_to raise_exception
    end

    it 'shall find apps on_period' do
      first_day = '01/01/2014'
      last_day = '01/01/2015'

      expect { Lead.on_period(first_day, last_day) }.not_to raise_exception
      expect { Lead.on_period(last_day, first_day) }.not_to raise_exception

    end

  end

end


