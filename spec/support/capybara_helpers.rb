require_relative '../spec_helper'

=begin
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end
=end

Capybara.default_driver = :webkit
Capybara.current_driver = :webkit
Capybara.javascript_driver = :webkit
Capybara.run_server = true #Whether start server when testing

Capybara.server_host = '127.0.0.1' # TODO
#Capybara.server_port = 3000
#Capybara.default_selector = :css #:xpath #default selector , you can change to :css
#Capybara.default_wait_time = 5 #When we testing AJAX, we can set a default wait time
Capybara.ignore_hidden_elements = false #Ignore hidden elements when testing, make helpful when you hide or show elements using javascript


