
SPL_STATUS_LIST = ["Анкета отклонялась ранее", "Не кредитуемый район / регион", "Нужен другой кредитный продукт (не кредитный продукт)",
                   "Возраст", "СТОП-Условие", "Не оставлял заявку, кредит не нужен", "Некорректные данные", "Недозвон",
                   "Принята (анкета заполнена)", "Принята", "Принята (кредит выдан)"]

namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    make_users
    make_leads
  end
end

def make_users
  users = []
  users << (User.create(email: "a.solodkov@prbb.ru", password: "12345678", access_token: "ffdsqwe123asewq1", partner_source: "admin"))
  users << (User.create(email: "davydenkov.mihail@gmail.com", password: "12345678", access_token: "ffdsqwe123asewq2", partner_source: "admin"))
  users << (User.create(email: "kuldoshina@gmail.com", password: "12345678", access_token: "ffdsqwe123asewq3", partner_source: "admin"))
  users << (User.create(email: "marikila84@gmail.com", password: "cvrl19qwerty", access_token: "ffdsqwe123asewq4", partner_source: "admin"))
#  User.create(email: "need4lead@gmail.com", password: "12345678", access_token: "ffdsqwe123asewq10", partner_source: "need4lead")

  users.each {|user| user.add_role :admin}

=begin
  10.times do |n|
    email = "getLead#{n+1}@gmail.com"
    password  = "12345678"
    access_token = "ffdsqwe123asewq"
    role_id = nil
    partner_source = "company name #{n+2}"

    User.create(email: email, password: password, access_token: access_token, role_id: role_id, partner_source: partner_source)
  end
=end
end

def make_leads
  partner_sources = User.all.pluck(:partner_source).uniq.compact - ["admin"]
  lead_status_list = ["Прошла валидацию", "Дубль", "Дубль собственный", "Ошибка валидации"]
  description_list = ["Анкета полностью соответствует требованиям",
      "Анкета содержит корректные данные, но телефонный номер уже поступал ранее в базу от другого партнёра",
      "Анкета содержит корректные данные, но телефонный номер уже поступал ранее в базу от данного партнёра",
      "Анкета заполнена некорректно. Обязательные поля отсутствуют или содержат недопустимые символы"
      ]


=begin
  1000.times do
    fio = Faker::Name.name
    phone = ("9" + "#{rand(999999999)}")
    city = Faker::Address.city
    region = city
    access_token = Faker::Lorem.characters(30)
    partner_source = partner_sources[rand(partner_sources.size)]
    is_test = true
    external_id = rand(10000000)
    traffic_source = Faker::Company.name
    ad_company = Faker::Company.name
    external_ip = "#{1 + rand(224)}.#{1 + rand(100)}.#{1 + rand(200)}.#{1 + rand(200)}"

    case rand(lead_status_list.size)
      when 0 then ( lead_status = lead_status_list[0]; description = description_list[0]; spl_status = SPL_STATUS_LIST[rand(SPL_STATUS_LIST.size)] )
      when 1 then ( lead_status = lead_status_list[1]; description = description_list[1] )
      when 2 then ( lead_status = lead_status_list[2]; description = description_list[2] )
      when 3 then ( lead_status = lead_status_list[3]; description = description_list[3] )
    end
    amount = 5001 + rand(10000)
    product_id = 1

    created_at = Time.current.to_datetime - rand(14).days

    Lead.create(fio: fio, phone: phone, city: city, region: region, access_token: access_token,
                is_test: is_test, created_at: created_at, partner_source: partner_source,
                external_id: external_id, traffic_source: traffic_source, ad_company: ad_company, external_ip: external_ip,
                spl_status: spl_status, lead_status: lead_status, description: description, amount: amount, product_id: product_id)
end
=end


end
