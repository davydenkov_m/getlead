# -*- encoding : utf-8 -*-
class CreateLeads < ActiveRecord::Migration
  def change
    create_table :leads do |t|
      t.string :fio
      t.string :phone
      t.string :city
      t.string :region
      t.string :access_token

      t.timestamps
    end
    add_index :leads, :phone, unique: true
  end
end
