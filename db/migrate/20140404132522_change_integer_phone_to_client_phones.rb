class ChangeIntegerPhoneToClientPhones < ActiveRecord::Migration
  def change
    remove_column :client_phones, :phone
    add_column :client_phones, :phone, :integer, limit: 8
  end
end
