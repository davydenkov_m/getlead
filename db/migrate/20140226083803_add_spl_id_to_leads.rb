# -*- encoding : utf-8 -*-
class AddSplIdToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :id_in_spl, :integer
  end
end
