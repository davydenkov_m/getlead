class AddExternalIpToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :external_ip, :string
  end
end
