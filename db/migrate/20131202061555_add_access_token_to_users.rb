# -*- encoding : utf-8 -*-
class AddAccessTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :access_token, :string
  end
end
