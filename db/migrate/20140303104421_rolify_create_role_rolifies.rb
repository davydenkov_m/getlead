# -*- encoding : utf-8 -*-
class RolifyCreateRoleRolifies < ActiveRecord::Migration
  def change
    create_table(:role_rolifies) do |t|
      t.string :name
      t.references :resource, :polymorphic => true

      t.timestamps
    end

    create_table(:users_role_rolifies, :id => false) do |t|
      t.references :user
      t.references :role_rolify
    end

    add_index(:role_rolifies, :name)
    add_index(:role_rolifies, [ :name, :resource_type, :resource_id ])
    add_index(:users_role_rolifies, [ :user_id, :role_rolify_id ])
  end
end
