# -*- encoding : utf-8 -*-
class AddParterIdToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :partner_id, :string
  end
end
