# -*- encoding : utf-8 -*-
class AddTestAttributeForLeads < ActiveRecord::Migration
  def change
    add_column :leads, :is_test, :boolean
  end
end
