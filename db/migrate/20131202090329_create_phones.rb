# -*- encoding : utf-8 -*-
class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :partner
      t.string :phone

      t.timestamps
    end
  end
end
