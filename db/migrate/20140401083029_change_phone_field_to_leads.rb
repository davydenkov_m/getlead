class ChangePhoneFieldToLeads < ActiveRecord::Migration
  def change
    remove_column :leads, :phone
    remove_column :phones, :phone

    add_column :leads, :phone, :integer, limit: 8
    add_column :phones, :phone, :integer, limit: 8
  end
end
