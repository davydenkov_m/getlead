# -*- encoding : utf-8 -*-
class AddExternalIdToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :external_id, :string
    add_column :leads, :traffic_source, :string
    add_column :leads, :ad_company, :string
  end
end
