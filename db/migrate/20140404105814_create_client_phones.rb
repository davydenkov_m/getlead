class CreateClientPhones < ActiveRecord::Migration
  def change
    create_table :client_phones do |t|
      t.integer :phone
      t.string :title

      t.timestamps
    end
  end
end
