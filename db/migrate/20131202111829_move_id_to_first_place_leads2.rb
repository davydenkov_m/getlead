# -*- encoding : utf-8 -*-
class MoveIdToFirstPlaceLeads2 < ActiveRecord::Migration
  def change
    remove_column :leads, :id
    remove_column :leads, :fio
    remove_column :leads, :phone
    remove_column :leads, :city
    remove_column :leads, :region
    remove_column :leads, :access_token
    remove_column :leads, :created_at
    remove_column :leads, :updated_at

    add_column :leads, :id, :uuid, default: 'uuid_generate_v1()'
    add_column :leads, :fio, :string, null: false
    add_column :leads, :phone, :string, null: false
    add_column :leads, :city, :string, null: false
    add_column :leads, :region, :string
    add_column :leads, :access_token, :string, null: false
    add_column :leads, :created_at, :string
    add_column :leads, :updated_at, :string
  end
end
