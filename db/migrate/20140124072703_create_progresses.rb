# -*- encoding : utf-8 -*-
class CreateProgresses < ActiveRecord::Migration
  def change
    create_table :progresses do |t|
      t.string :value
      t.integer :user_id
      t.string :source
      t.integer :model

      t.timestamps
    end
  end
end
