# -*- encoding : utf-8 -*-
class EnableUuidOsspExtension < ActiveRecord::Migration
  def change
    enable_extension 'uuid-ossp'
    remove_column :leads, :id
    add_column :leads, :id, :uuid, default: 'uuid_generate_v1()'
  end
end
