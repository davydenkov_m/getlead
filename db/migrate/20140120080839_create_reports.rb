# -*- encoding : utf-8 -*-
class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :title
      t.integer :user_id
      t.string :url

      t.timestamps
    end
  end
end
