# -*- encoding : utf-8 -*-
class ChangePartnerIdField < ActiveRecord::Migration
  def change
    remove_column :users, :partner_id
    remove_column :leads, :partner_id

    add_column :users, :partner_source, :string
    add_column :leads, :partner_source, :string
  end
end
