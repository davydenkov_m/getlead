# -*- encoding : utf-8 -*-
class AddPartnerIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :partner_id, :string
  end
end
