# -*- encoding : utf-8 -*-
class AddLeadStatusToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :lead_status, :string
  end
end
