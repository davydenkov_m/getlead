# -*- encoding : utf-8 -*-
class AddDescriptionToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :description, :string
  end
end
