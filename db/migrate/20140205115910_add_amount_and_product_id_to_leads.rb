# -*- encoding : utf-8 -*-
class AddAmountAndProductIdToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :amount, :integer
    add_column :leads, :product_id, :integer
  end
end
