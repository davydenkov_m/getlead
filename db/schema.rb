# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140422123734) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "client_phones", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "phone",      limit: 8
  end

  create_table "leads", id: false, force: true do |t|
    t.uuid     "id",                       default: "uuid_generate_v1()"
    t.string   "fio",                                                     null: false
    t.string   "city",                                                    null: false
    t.string   "region"
    t.string   "access_token",                                            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "lead_status"
    t.boolean  "is_test"
    t.string   "external_id"
    t.string   "traffic_source"
    t.string   "ad_company"
    t.string   "description"
    t.string   "partner_source"
    t.integer  "amount"
    t.integer  "product_id"
    t.integer  "spl_status_id"
    t.integer  "id_in_spl"
    t.string   "external_ip"
    t.string   "spl_status"
    t.integer  "phone",          limit: 8
  end

  create_table "notifications", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "phones", force: true do |t|
    t.string   "partner"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "phone",      limit: 8
  end

  create_table "progresses", force: true do |t|
    t.string   "value"
    t.integer  "user_id"
    t.string   "source"
    t.integer  "model"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: true do |t|
    t.string   "title"
    t.integer  "user_id"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "role_rolifies", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "role_rolifies", ["name", "resource_type", "resource_id"], name: "index_role_rolifies_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "role_rolifies", ["name"], name: "index_role_rolifies_on_name", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "access_token"
    t.string   "partner_source"
    t.integer  "status",                 default: 0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_role_rolifies", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_rolify_id"
  end

  add_index "users_role_rolifies", ["user_id", "role_rolify_id"], name: "index_users_role_rolifies_on_user_id_and_role_rolify_id", using: :btree

end
