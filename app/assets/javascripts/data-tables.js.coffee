jQuery ->
  $.extend $.fn.dataTableExt.oStdClasses,
    sSortAsc: "header headerSortDown"
    sSortDesc: "header headerSortUp"
    sSortable: "header"
  $('#leads_table').dataTable
    #sDom: "<'row'<'span6'><'span6'f>r>t<'row'<'span6'><'span6'p>>"
    sDom: "<'clear'>lftipr"
    sPaginationType: "bootstrap"
    bProcessing: true
    bServerSide: true
    bScrollCollapse: true
    iDisplayLength: 25
    bDestroy: true
    sAjaxSource: $('#leads_table').data('source')
    fnRowCallback: ( nRow, aData, iDisplayIndex ) ->
      $(nRow).addClass(aData[9])
    oLanguage:
      sLengthMenu: "_MENU_ кол-во для отображения"
      sLoadingRecords: "Загрузка данных ..."
      sSearch: "Поиск: "
      sProcessing: 'Загрузка данных ...'
