function progress(){
    var progress = setInterval(function() {
        $.get("/progresses/1.json", function(data) {
            console.log(data.value);
            if (data.value == "done") {
                NProgress.done();
                location.reload();
            } else {
                NProgress.inc();
                //NProgress.set(data.value);
            };

        });
    }, 1000);
    event.preventDefault();
}


function addNprogress() {

    $('input[id=download_to_xlsx], input[id=download_to_csv], input[id=download_to_xlsx_processed], input[id=download_to_csv_processed]').on('click', function() {


        var nprogress = $('<div id="nprogress" style="display: none; height: 22px; "><span>Выполняется выгрузка...</span><div class="bar" role="bar"><div class="peg" style="height: 100%;"></div></div></div>');
        if ($('#nprogress').length === 0) {
            nprogress.insertBefore($('#reports'));
        };

        NProgress.start();
        $('#nprogress').hide().fadeIn('fast');

        $.get("/progresses/1.json", function(data) {
            //if ((data.value != "done")) {
            console.log('ajax here');
            progress();
            //}
        });

    });
};
