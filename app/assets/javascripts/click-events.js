$(document).ready(function(){

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }


    addDatepicker();

    $(".selectpicker").selectpicker();

    // ONCLICK EVENTS

    $('#excel_download').on('click', function() {
        if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null && $("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null) {
            $('input[id=download_to_xlsx]').click();
        } else {
            if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_first").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
            if ($("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_last").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
        }
    });

    $('#csv_download').on('click', function() {
        if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null && $("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null) {
            $('input[id=download_to_csv]').click();
        } else {
            if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_first").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
            if ($("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_last").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
        }
    });

    $('#excel_download_processed').on('click', function() {
        if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null && $("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null) {
            $('input[id=download_to_xlsx_processed]').click();
        } else {
            if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_first").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
            if ($("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_last").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
        }
    });

    $('#csv_download_processed').on('click', function() {
        if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null && $("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null) {
            $('input[id=download_to_csv_processed]').click();
        } else {
            if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_first").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
            if ($("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_last").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
        }
    });

    $('#show_stat').on('click', function() {
        if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null && $("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) != null) {
            $('input[id=show_apps]').click();
        } else  {
            if ($("input#created_at_first").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_first").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
            if ($("input#created_at_last").val().match(/^[0-3]\d\/[0-1]\d\/[1-2]\d\d\d$/) == null) { $("input#created_at_last").popover({animation: true, placement: "bottom", trigger: "manual" }).popover('show'); };
        }
    });

    $("input#created_at_first").on('click', function() {
        $("input#created_at_first").popover({animation: true, placement: "bottom", trigger: "manual", delay: { show: 1000, hide: 0 }}).popover('hide');
    });

    $("input#created_at_last").on('click', function() {
       $("input#created_at_last").popover({animation: true, placement: "bottom", trigger: "manual", delay: { show: 1000, hide: 0 }}).popover('hide');
    });

    $("#add_partner_button").on('click', function() {
      $("#add_button").click();
    });

    $("#update_partner_button").on('click', function() {
        $("#update_button").click();
    });

    // SET NICE TEXT

    $("#select_fields").closest("div").find(".filter-option").text("Поля для выгрузки");
    $("#select_client_id").closest("div").find(".filter-option").text("Партнёр");

    $("#checked_partners").on('click', function() {
      $("#select_client_id").selectpicker('selectAll');
    });

    $(".close").on('click', function () {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode.parentNode.parentNode);
        alert(document.cookie);
        alert(getCookie('notification_ids'));
        return false;
    });

/*    appsChart();*/

    addNprogress();

});


