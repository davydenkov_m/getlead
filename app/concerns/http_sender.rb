module HttpSender
  def self.included(base)
    base.extend(HttpSenderOptions)
  end

  def send_request(id, uri, *args, &block)
    Rails.env.development? ? ( uri = URI.parse(uri);  http = Net::HTTP.new(uri.host, uri.port,  ENV['PROXY'], ENV['PROXY_PORT']) )
                           : ( uri = URI.parse(uri); http = Net::HTTP.new(uri.host, uri.port) )
    unless uri.to_s.scan('https').empty?
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    body_to_send = params_to_send(id, *args, &block)
    request = eval(http_method).new("#{uri.request_uri}")
    request.basic_auth basic_auth_login, basic_auth_password  if (basic_auth_login && basic_auth_password)
    request.set_form_data(body_to_send)
    request["Content-Type"] = "application/x-www-form-urlencoded"
    http.request(request)
  end

  def method_missing(method, *args, &block)
    case method
      when 'http_method'.to_sym then Net::HTTP::Post
      when 'basic_auth_login'.to_sym then nil
      when 'basic_auth_password'.to_sym then nil
      else super
    end
  end

  module HttpSenderOptions
    def http_sender_options(args)
      args.each {|method, v|
        define_method "#{method}".to_sym do
          __callee__.to_s == "http_method" ? "Net::HTTP::#{v.capitalize}" : v
        end
      }
    end
  end
end
