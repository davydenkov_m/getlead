# -*- encoding : utf-8 -*-
class LeadsDatatable
  delegate :params, :session, :current_user, :h, :link_to, :number_to_currency, :content_tag, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Lead.count,
        iTotalDisplayRecords: leads.total_entries,
        aaData: data
    }
  end

  private

  def data
    leads.map do |lead|
      [
          # lead.id || "-",
          # lead.external_id || "-",
          lead.fio || "-",
          lead.phone || "-",
          lead.city || "-",
          # lead.region || "-",
          lead.lead_status || "-",
          lead.spl_status || "-",
          # lead.description || "-",
          # lead.traffic_source || "-",
          # lead.ad_company || "-",
          lead.partner_source || "-",
          # lead.amount || "-",
          lead.created_at.strftime("%d.%m %H:%M:%S")  || "-",
          if lead.updated_at.to_i != lead.created_at.to_i
            lead.updated_at.strftime("%d.%m %H:%M:%S")
          else
            ""
          end,
          link_to(lead, { :remote => true, 'data-toggle' =>  'lead-modal' }) do
            content_tag :i, '', class: 'icon-eye-open'
            end,
          set_class_by_status(lead.lead_status, lead.spl_status)
      ]
    end
  end

  def leads
    @leads ||= fetch_leads
  end

  def fetch_leads
    leads = Lead.on_period(session[:created_at_first], session[:created_at_last]).find_partners(session[:client_ids], current_user.admin?).order("#{sort_column} #{sort_direction}")
    leads = leads.page(page).per_page(per_page)

    if params[:sSearch].present?
      case
        when params[:sSearch].length == 10 && params[:sSearch] =~ /^\d+$/
          leads = leads.where('phone = :search', search: params[:sSearch].to_i)
        when params[:sSearch].length >= 3
          leads = leads.where('partner_source = :search or id = :search', search: params[:sSearch])
      end
    end

    leads
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 100
  end

  def sort_column
    columns = %w[fio phone city lead_status spl_status partner_source created_at updated_at]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end

=begin
  def fresh(status_id)
    status_id == 7
  end
=end

  def set_updated_at


  end

  def set_class_by_status(status, spl_status)
    case status
      when 'Прошла валидацию'
        if ["Принята (анкета заполнена)", "Принята", "Принята (кредит выдан)"].include?(spl_status)
          'spl_positive'
        else
          'spl_negative'
        end
      when 'Дубль'
        'not_valid_light'
      when 'Дубль собственный'
        'not_valid_light'
      else
        'not_valid_light'
    end
  end
end

