# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  rolify :role_cname => 'RoleRolify'
  enum status: [:test, :active]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates :email, :password, :access_token, :partner_source, presence: true
  validates :access_token, :email, uniqueness: true

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :leads, foreign_key: 'partner_source', primary_key: :partner_source, dependent: :destroy

  # Setup accessible (or protected) attributes for your model
  # attr_accessible :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body

  def admin?
    self.has_role? :admin
  end
end
