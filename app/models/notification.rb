class Notification < ActiveRecord::Base

  def self.current(hidden_ids = nil)
    result = where('Date(created_at) = ?', Date.today)
    result = result.where("id not in (?)", hidden_ids) if hidden_ids.present?
    result
  end

end
