# -*- encoding : utf-8 -*-
require 'active_support'

class Lead < ActiveRecord::Base
  acts_as_xlsx
  belongs_to :user, primary_key: :partner_source, foreign_key: 'partner_source' # primary for user and foreign for
  self.primary_key = :id

  VALID_RUSSIAN_REGEXP = /\A[а-яА-ЯЁё\s\-]*\z/
  VALID_PHONE_REGEXP = /\A9\d{9}\z/
  validates :fio, :city, :phone, :access_token, presence: true
  validates :phone, presence: true, format: { with: VALID_PHONE_REGEXP }
  validates :fio, :city, format: {with: VALID_RUSSIAN_REGEXP}

  after_commit :send_to_SPL, on: :create, if: -> { self.lead_status == "Прошла валидацию" && self.user.active? }

  LEAD_STATUS_LIST = ["Прошла валидацию", "Ошибка валидации", "Дубль", "Дубль собственный"]

  SPL_STATUS_LIST = ["Анкета отклонялась ранее", "Не кредитуемый район / регион", "Нужен другой кредитный продукт (не кредитный продукт)",
                 "Возраст", "СТОП-Условие", "Не оставлял заявку, кредит не нужен", "Некорректные данные", "Недозвон",
                 "Принята (анкета заполнена)", "Принята", "Принята (кредит выдан)"]

  protected

  def self.set_counters(first_day, last_day, client_ids, admin)
    args = {first_day: first_day, last_day: last_day, client_ids: client_ids, admin?: admin}

    h = {
        total: Lead.find_partners(client_ids, admin).apps_by_day(first_day, last_day),
        total_processed: Lead.find_partners(client_ids, admin).apps_processed_by_day(first_day, last_day),
        accepted:    Lead.count_lead_status(0, args),
        not_valid:   Lead.count_lead_status(1, args),
        repeat:      Lead.count_lead_status(2, args),
        repeat_self: Lead.count_lead_status(3, args),

        spl_status_1: Lead.count_spl_status(0, args),
        spl_status_2: Lead.count_spl_status(1, args),
        spl_status_3: Lead.count_spl_status(2, args),
        spl_status_4: Lead.count_spl_status(3, args),
        spl_status_5: Lead.count_spl_status(4, args),
        spl_status_6: Lead.count_spl_status(5, args),
        spl_status_7: Lead.count_spl_status(6, args),
        spl_status_8: Lead.count_spl_status(7, args),
        spl_status_9: Lead.count_spl_status(8, args),
        spl_status_10: Lead.count_spl_status(9, args),
        spl_status_11: Lead.count_spl_status(10, args),
        not_accepted: {}
    }

    h[:total].each_pair {|key, value|
      h[:accepted].keys.include?(key) ? (h[:not_accepted][key] = value - h[:accepted][key])
      : (h[:not_accepted][key] = value)
    }
    h
  end

  ### SCOPES

  scope :find_partners,
    ->(client_ids, admin) { admin ? where('partner_source in (:client_ids)', client_ids: client_ids)
                                  : where('partner_source = :id', id: client_ids)
                          }


  scope :status,
    ->(status) { where('lead_status = :status',status: status) }

  scope :on_period,
    #where('created_at >= :first_day and created_at <= :last_day', first_day: first_day, last_day: last_day)
    ->(first_day, last_day) { where('created_at >= :first_day and created_at <= :last_day', first_day: first_day, last_day: last_day) }


  def self.count_spl_status(spl_status_id, args)
    find_partners(args[:client_ids], args[:admin?]).where(spl_status: "#{SPL_STATUS_LIST[spl_status_id]}").apps_processed_by_day(args[:first_day], args[:last_day])
  end

  def self.count_lead_status(lead_status_id, args)
    find_partners(args[:client_ids], args[:admin?]).where(lead_status: "#{LEAD_STATUS_LIST[lead_status_id]}").apps_by_day(args[:first_day], args[:last_day])
  end

  def self.apps_processed_by_day(first_day, last_day)
    leads = self.on_period(first_day, last_day).select("updated_at, partner_source, lead_status").order(updated_at: :asc).group_by { |t| t.updated_at.to_date }
    apps = {}
    leads.each_pair {|key, value| apps[key] = value.count}
    return apps
  end

  def self.apps_by_day(first_day, last_day)
    leads = self.on_period(first_day, last_day).select("created_at, partner_source, lead_status").order(created_at: :asc).group_by { |t| t.created_at.to_date }
    apps = {}
    leads.each_pair {|key, value| apps[key] = value.count}
    return apps
  end



  #### END SCOPES

  ### CHART SECTION

=begin
  def self.chart_data(args)
    first_day = args[:first_day] || 1.month.ago
    last_day = args[:last_day] || Time.zone.now
    current_user = args[:current_user]
    counters = args[:counters]

    apps_total_count = counters[:total]; apps_accepted_count = counters[:accepted]; apps_not_valid = counters[:not_valid]; apps_repeat = counters[:repeat]; apps_repeat_self = counters[:repeat_self]
    apps_not_accepted_count = counters[:not_accepted]

    (first_day.to_date..last_day.to_date).map do |date|
      {
          created_at: date,
          apps_total: apps_total_count[date] || 0,
          apps_accepted: apps_accepted_count[date] || 0,
          apps_not_accepted: apps_not_accepted_count[date] || 0,
          apps_not_valid: apps_not_valid[date] || 0,
          apps_repeat: apps_repeat[date] || 0,
          apps_repeat_self: apps_repeat_self[date] || 0,
      }
    end
  end
=end

  ### END CHART

  private

  def send_to_SPL
    Rails.env.development? ? ToSplWorker.perform_async(self.id, "http://192.168.64.17/api/leads")
                           : ToSplWorker.perform_async(self.id, "http://195.151.135.103/api/leads")
  end

end
