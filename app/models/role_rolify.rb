# -*- encoding : utf-8 -*-
class RoleRolify < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_role_rolifies
  belongs_to :resource, :polymorphic => true
  
  scopify
end
