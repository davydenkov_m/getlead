# -*- encoding : utf-8 -*-

class LeadSerializer < ActiveModel::Serializer
  attributes :id, :external_id, :external_ip, :fio, :phone, :city, :region, :lead_status, :spl_status, :description, :created_at, :traffic_source, :ad_company, :amount, :product_id

  def attributes
    data = super
    data[:spl_status] = "Не обрабатывалась" unless object.spl_status
    data[:phone] = Digest::SHA256.base64digest(object.phone.to_s).truncate(15)
    data
  end

end
