json.array!(@progresses) do |progress|
  json.extract! progress, :id, :value, :user_id, :source, :model
  json.url progress_url(progress, format: :json)
end
