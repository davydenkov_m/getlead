json.array!(@reports) do |report|
  json.extract! report, :id, :title, :user_id, :url
  json.url report_url(report, format: :json)
end
