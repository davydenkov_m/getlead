# -*- encoding : utf-8 -*-
class LeadDecorator

  attr_reader :counters

  attr_reader :total_count_hash, :total_processed_count_hash, :accepted_count_hash, :not_accepted_count_hash,
              :not_valid_count_hash, :repeat_count_hash, :repeat_self_count_hash,
              :spl_status_1_count_hash, :spl_status_2_count_hash, :spl_status_3_count_hash,
              :spl_status_4_count_hash, :spl_status_5_count_hash, :spl_status_6_count_hash,
              :spl_status_7_count_hash, :spl_status_8_count_hash, :spl_status_9_count_hash, :spl_status_10_count_hash,
              :spl_status_11_count_hash

  def initialize(args)
    @current_user = args[:current_user]
    @client_ids = args[:client_ids]
    @session = args[:session]
    @counters = args[:counters]

    set_statistics
  end

  def set_statistics
    set_total_counters
  end

  def set_total_counters
    @total_count_hash = counters[:total]
    @total_processed_count_hash = counters[:total_processed]
    @accepted_count_hash = counters[:accepted]
    @not_accepted_count_hash = counters[:not_accepted]
    @not_valid_count_hash = counters[:not_valid]
    @repeat_count_hash = counters[:repeat]
    @repeat_self_count_hash = counters[:repeat_self]

    @spl_status_1_count_hash = counters[:spl_status_1]
    @spl_status_2_count_hash = counters[:spl_status_2]
    @spl_status_3_count_hash = counters[:spl_status_3]
    @spl_status_4_count_hash = counters[:spl_status_4]
    @spl_status_5_count_hash = counters[:spl_status_5]
    @spl_status_6_count_hash = counters[:spl_status_6]
    @spl_status_7_count_hash = counters[:spl_status_7]
    @spl_status_8_count_hash = counters[:spl_status_8]
    @spl_status_9_count_hash = counters[:spl_status_9]
    @spl_status_10_count_hash = counters[:spl_status_10]
    @spl_status_11_count_hash = counters[:spl_status_11]

  end

  def dates_for_statistic
    total_count_hash.keys rescue []
  end

  def processed_dates
    total_processed_count_hash.keys rescue []
  end

  def total_count
    total_count_hash.values
  end

  def total_processed_count
    hash = {}
    total_processed_count_hash.each{|date, count|
      (1..11).each {|e|
        hash[date] ||= 0
        hash[date] += send(:"spl_status_#{e}_count_hash")[date] if send(:"spl_status_#{e}_count_hash").include?(date)
      }
    }
    hash.values
  end

  def total_count_summ
    total_count.reduce(&:+)
  end

  def total_processed_count_summ
    total_processed_count.reduce(&:+)
  end


  (1..11).each { |e|  ### SPL_STATUS DECORATOR METHODS
    define_method("spl_status_#{e}_dates") {        ### dates methods
      send(:"spl_status_#{e}_count_hash").keys rescue []
    }

    define_method("spl_status_#{e}_count") {        ### Count methods
      send(:"spl_status_#{e}_count_hash").values
    }

    define_method("spl_status_#{e}_count_summ") {        ### Count methods
      send(:"spl_status_#{e}_count_hash").values.reduce(&:+)
    }

    define_method("diff_in_dates_spl_status_#{e}") { ### difference in dates for table rendering methods
      dates_for_statistic - send(:"spl_status_#{e}_dates")
    }

    define_method("diff_in_processed_dates_spl_status_#{e}") { ### difference in dates for table rendering methods
      processed_dates - send(:"spl_status_#{e}_dates")
    }
  }

  %w(accepted not_accepted repeat repeat_self not_valid).each { |e| ### SCORING STATUS DECORATOR METHODS
    define_method("#{e}_dates") {
      send("#{e}_count_hash").keys rescue []
    }

    define_method("diff_in_dates_#{e}") {
      dates_for_statistic - send(:"#{e}_dates")
    }

    define_method("#{e}_count") {
      send(:"#{e}_count_hash").values
    }

    define_method("#{e}_count_summ") {
      send(:"#{e}_count_hash").values.reduce(&:+)
    }
  }

  def method_missing

  end

  def respond_to_missing?

  end

end
