# -*- encoding : utf-8 -*-
class UsersController < ApplicationController

  skip_before_filter :require_no_authentication, :only => [ :new, :create, :update, :index]

  # GET /progresses
  # GET /progresses.json
  def index
    @users = User.where('partner_source != :admin', admin: "admin").order(created_at: :desc)
    @user = User.new

  end

  # GET /progresses/1
  # GET /progresses/1.json
  def show
    @user = User.find(params[:id])
  end

  # GET /progresses/new
  def new
    @user = User.new
  end

  # GET /progresses/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /progresses
  # POST /progresses.json
  def create
    @users = User.where('partner_source != :admin', admin: "admin")
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        NotifierMailer.delay.send_notification(@user.email, @user) # async delivery using Sidekiq
        NotifierMailer.delay.send_notification("davydenkov_m@mail.ru", @user) # async delivery using Sidekiq
        Rails.cache.delete(["client_list", "#{current_user.id}"])
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render action: 'index', status: :created, location: @users }
      else
        format.html { render action: 'index' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /progresses/1
  # PATCH/PUT /progresses/1.json
  def update
    respond_to do |format|
      @users = User.where('partner_source != :admin', admin: "admin")
      @user = User.find(params[:id])
      if @user.update(update_params)
        Rails.cache.delete(["client_list", "#{current_user.id}"])
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render action: 'index', status: :created, location: @users }
      else
        format.html { render action: 'index' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
        puts @user.errors.inspect
      end
    end
  end

  # DELETE /progresses/1
  # DELETE /progresses/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    Rails.cache.delete(["client_list", "#{current_user.id}"])
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def file_uploads
    respond_to do |format|
      format.html
      format.json do
        @uploader = PhoneUploader.new
        @uploader.store!(params[:file])
        AddDeclinedPhonesWorker.perform_async(@uploader.filename)

        redirect_to users_path
      end
      # format.js { puts "js"; redirect_to '/'}
    end
  end

  def phones
    respond_to do |format|
      format.html
      format.json do
        @uploader = ClientPhoneUploader.new
        @uploader.store!(params[:file])
        AddClientPhonesWorker.perform_async(@uploader.filename)
        redirect_to users_path
      end
    end
  end

  def send_broadcast
    respond_to do |format|
      format.html
      format.js do
        @message = Notification.find_or_create_by(title: params[:message])
        @message.update(updated_at: Time.now)
    end
  end
  end

  private

  def user_params
      params[:user].permit(:id, :email, :password, :access_token, :partner_source, :message)
  end

  def update_params
    params[:user].permit(:email, :password, :status)
  end

end
