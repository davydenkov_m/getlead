# -*- encoding : utf-8 -*-

module Api
  module V2
    class LeadsController < ApplicationController # Api::BaseController
      before_action :restrict_access, only: [:create, :statistics]
      before_action :check_spl, only: [:update]

      respond_to :json

      def index
        respond_to do |format|
          format.html {render text: "Your data was sucessfully loaded. Thanks"}
          format.json { render text: Lead.first.to_json }
        end
      end

      def show
        respond_with Lead.find(params[:id])
      end

      def create ## from credit.banklife.org and create from POST
        fio = params[:fio].force_encoding('utf-8') rescue nil
        city = params[:city].force_encoding('utf-8') rescue nil
        region = params[:region].force_encoding('utf-8') rescue nil

        @last_lead = Lead.new(access_token: params[:access_token], city: city, created_at: Time.now, fio: fio, phone: params[:phone].to_i, partner_source: params[:partner_source],
                              region: region, updated_at: Time.now, is_test: params[:test], external_id: params[:external_id], traffic_source: params[:traffic_source], ad_company: params[:ad_company],
                              amount: params[:amount], product_id: params[:product_id], external_ip: params[:external_ip])
        check_phone_and_save
      end

      def update ## respond to SPL request
        scoring_uid = params.fetch(:scoring_uid) {"N/A"}

        @last_lead = Lead.find_by(id: scoring_uid)
        @last_lead ? ( set_status(:updated); @last_lead.save!; send_json(updated: true) )
                   : ( send_json(not_updated: true) )
      end

      def statistics
        head :bad_request unless params[:partner_source] && params[:access_token]
        params[:from] ||= DateTime.parse(2.weeks.ago.to_s)
        params[:to]   ||= DateTime.parse(Time.zone.now.to_s)

        respond_to do |format|
          format.json {
            render json: Lead.find_partners(client_id=params[:partner_source], admin=false)
                             .on_period(first_day=DateTime.parse("#{params[:from]}"), last_day=DateTime.parse("#{params[:to]}")), root: false
          }
        end
      end

      private

      def is_partner?
        access_token = request.headers['HTTP_ACCESS_TOKEN'] || params[:access_token]
        @partner = User.find_by(access_token: access_token, partner_source: params[:partner_source])
        @partner.present?
      end

      def client_phone?
        ClientPhone.where(phone: params[:phone].to_i).exists?
      end

      def phone_update_and_save
        @phone.partner = params[:partner_source]
        @phone.save
      end

      def check_spl
        head :unauthorized unless params[:access_token] == "ffdsqwe123asewq"
      end

      def check_phone_and_save
        if client_phone?
          save_lead_if_valid
        else
          @phone = Phone.where('created_at > ?', 3.months.ago)
                  .find_or_initialize_by(phone: params[:phone].to_i)
          @phone.new_record? ? ( phone_update_and_save; save_lead_if_valid )
                             : ( already_saved )
        end
      end

      def restrict_access
        head :unauthorized unless is_partner?
      end

      def save_lead_if_valid
        set_status(:accepted)

        if @last_lead.save
          send_json(valid: true)
        else
          set_status(:not_valid)
          send_json(not_valid: true)
        end
      rescue => e
        send_json(not_valid: true)
      end

      def already_saved
        @phone.partner == params[:partner_source] ? (set_status(:repeat_self))
                                                  : (set_status(:repeat))
        if !@last_lead.save
          set_status(:not_valid)
        end

        case @last_lead.lead_status
          when 'Дубль' then send_json(already_saved: "Дубль")
          when 'Дубль собственный' then send_json(already_saved: "Дубль собственный")
          when 'Ошибка валидации' then send_json(not_valid: true)
        end

      end

      def set_status(status)
        case status
          when :accepted then ( @last_lead.lead_status = "Прошла валидацию"; @last_lead.description = "Анкета полностью соответствует требованиям"; @last_lead.access_token = "ffdsqwe123asewq" )
          when :repeat then ( @last_lead.lead_status = "Дубль"; @last_lead.description = "Анкета содержит корректные данные, но телефонный номер уже поступал ранее в базу от другого партнёра" )
          when :repeat_self then ( @last_lead.lead_status = "Дубль собственный"; @last_lead.description = "Анкета содержит корректные данные, но телефонный номер уже поступал ранее в базу от данного партнёра" )
          when :not_valid then ( @last_lead.lead_status = "Ошибка валидации"; @last_lead.description = "Анкета заполнена некорректно. Обязательные поля отсутствуют или содержат недопустимые символы" )
          when :updated then ( @last_lead.spl_status_id = params[:spl_status_id].to_i; @last_lead.id_in_spl = params[:id_in_spl].to_i; @last_lead.spl_status = params[:spl_status] )
        end
      end

      def send_json(options = {})
        respond_to do |format|
          format.json {
             ( render text: "статус - #{@last_lead.lead_status}, внешний ID заявки - #{@last_lead.external_id}, внутренний ID - #{@last_lead.id}".to_json ) if options[:valid]
             ( render text: "Анкета заполнена некорректно. Обязательные поля отсутствуют или содержат недопустимые символы".to_json ) if options[:not_valid]
             ( render text: options[:already_saved].to_json ) if options[:already_saved]

             ( render text: "Статус заявки обновлён".to_json ) if options[:updated]
             ( render text: "Невозможно найти заявку в Scoring".to_json ) if options[:not_updated]
          }
        end
      end

    end
  end
end
