# -*- encoding : utf-8 -*-
class LeadsController < ApplicationController
  before_action :set_lead, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  attr_reader :counters, :client_ids

  # GET /leads
  def index
    set_initial_params
    @lead_decorator = LeadDecorator.new(args_for_decorator)
        respond_to do |format|
          format.html
          format.csv { send_data @leads.to_csv_modified }
          format.xlsx # { send_data @contacts.to_xlsx }#{ render :xlsx => "index", :filename => "all_contacts.xlsx" }
          format.js
          format.json { render json: LeadsDatatable.new(view_context) }
        end
  end

  # GET /leads/1
  def show
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /leads/new
  def new
    @lead = Lead.new
  end

  # GET /leads/1/edit
  def edit
  end

  # POST /leads
  def create
      @lead = Lead.new(lead_params)
        if @lead.save
          redirect_to @lead
        else
          render action: 'new'
        end
  end

  # PATCH/PUT /leads/1
  def update
    if @lead.update(lead_params)
      redirect_to @lead
    else
      render action: 'edit'
    end
  end

  # DELETE /leads/1
  def destroy
    @lead.destroy
    redirect_to leads_url
  end

  def import
    set_initial_params
    @lead_decorator = LeadDecorator.new(args_for_decorator)

    #@phones = Phone.where(partner: current_user.partner_source).where('phones.created_at >= ?', session[:created_at_first]).where('(phones.created_at) <= ?', session[:created_at_last]).paginate(:page => params[:page], :per_page => 30)
    excel_processed = "Выгрузить обработанные данные в формате Excel"
    csv_processed = "Выгрузить обработанные данные в формате CSV"

    params[:only_processed] = true if (params[:commit] == excel_processed) || (params[:commit] == csv_processed)

    case params[:commit]
      when "Выгрузить в формате Excel" then
        ReportsWorker.perform_async("excel", session[:lead_ids], @fields_for_uploading, current_user.id, current_user.partner_source, session[:created_at_first], session[:created_at_last], session[:client_ids])
      when "Выгрузить в формате CSV" then
        ReportsWorker.perform_async("csv", session[:lead_ids], @fields_for_uploading, current_user.id, current_user.partner_source, session[:created_at_first], session[:created_at_last], session[:client_ids])
      when excel_processed
        ReportsWorker.perform_async("excel", session[:lead_ids], @fields_for_uploading, current_user.id, current_user.partner_source, session[:created_at_first], session[:created_at_last], session[:client_ids], params[:only_processed])
      when csv_processed
        ReportsWorker.perform_async("csv", session[:lead_ids], @fields_for_uploading, current_user.id, current_user.partner_source, session[:created_at_first], session[:created_at_last], session[:client_ids], params[:only_processed])
      when "Показать статистику"
        respond_to do |format|
          format.js { render action: "index" } #
        end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lead
      @lead = Lead.find(params[:id])
    end

    def args_for_decorator
      {current_user: current_user, client_ids: @client_ids, session: session, counters: counters}
    end

    # Only allow a trusted parameter "white list" through.
    def lead_params
    #  params.require(:leads).permit(:fio, :phone, :city, :region, :access_token)
    end

    def set_initial_params
      session[:created_at_first] = DateTime.parse(params[:created_at_first]).beginning_of_day if params[:created_at_first]
      session[:created_at_first] ||= DateTime.parse(2.weeks.ago.to_s)

      session[:created_at_last] = DateTime.parse(params[:created_at_last]).end_of_day if params[:created_at_last]
      session[:created_at_last] ||= DateTime.parse(Time.zone.now.to_s)

      @fields_for_uploading = {ID: "id", Внешний_ID: "external_id", Внешний_IP: "external_ip", ФИО: "fio", Телефон: "phone", Город: "city", Регион: "region",
                               Результат_обработки: "spl_status", Результат_проверки: "lead_status", Описание: "description",
                               Дата_создания: "created_at", Источник_трафика: "traffic_source", Рекламная_компания: "ad_company", Партнёр: "partner_source", Сумма: "amount", Продукт: "product_id", Is_test: "is_test" }

      current_user.admin? ? (@clients_list = ( Rails.cache.fetch(["client_list", "#{current_user.id}"], expires_in: 1.minutes) { User.pluck(:partner_source).uniq.compact - ["admin"] } ) )
                          : (@clients_list = ( Rails.cache.fetch(["client_list", "#{current_user.id}"], expires_in: 1.minutes) { current_user.partner_source } ) )

      @client_ids = params.fetch(:client_ids){session[:client_ids] } || @clients_list   ## define default if not found with {}

      current_user.admin? ? (session[:client_ids] = params.fetch(:client_ids){@client_ids} )
                          : (session[:client_ids] = current_user.partner_source )

      @lead_ids = params[:lead_ids] || session[:lead_ids] || %w[ID Внешний_ID Внешний_IP ФИО Телефон Город Регион Результат_обработки Результат_проверки Дата_создания Описание Источник_трафика Рекламная_компания Партнёр Сумма Продукт Is_test]
      session[:lead_ids] = params[:lead_ids] if params[:lead_ids]

      @reports = Report.where(user_id: current_user.id).order('created_at DESC').limit(3)

      @progress = Progress.find_or_create_by(user_id: current_user.id)
      @progress.update(value: "start")

      session[:current_user_id] = current_user.id

      collect_counters
    end

    def collect_counters
      cache_instance_var('@counters', expires_in: 15.minutes, for_current_user: true) { Lead.set_counters(session[:created_at_first], session[:created_at_last], session[:client_ids], current_user.admin?) }
    end

end
