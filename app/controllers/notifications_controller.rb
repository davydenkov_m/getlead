# -*- encoding : utf-8 -*-
class NotificationsController < ApplicationController

  def hide
    ids = [params[:id], *cookies.signed[:hidden_notification_ids]]
    cookies.permanent.signed[:hidden_notification_ids] = ids
  end

end