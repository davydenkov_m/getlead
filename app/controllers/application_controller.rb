# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  include MongodbLogger::Base

  protect_from_forgery

  private
    # Using a private method to encapsulate the permissible parameters is just a good pattern
    # since you'll be able to reuse the same permit list between create and update. Also, you
    # can specialize this method with per-user checking of permissible attributes.

  def cache_instance_var(var_name, options, &block)
    enable_cache_for_current_user = current_user.id if options[:for_current_user]
    if params[:commit]
      instance_variable_set(var_name, block.call)
      Rails.cache.write(
          "#{var_name}#{enable_cache_for_current_user}",
          instance_variable_get(var_name),
          expires_in: options[:expires_in]
      )
    else
      instance_variable_set(
          var_name,
          Rails.cache.fetch(
              "#{var_name}#{enable_cache_for_current_user}",
              expires_in: options[:expires_in]
          ) { block.call }
      )
    end
  end


  def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :remember_me)
    end

  end
