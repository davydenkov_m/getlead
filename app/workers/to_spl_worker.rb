# -*- encoding : utf-8 -*-
class ToSplWorker
  include HttpSender
  include Sidekiq::Worker

  sidekiq_options(queue: :high, backtrace: true)
  http_sender_options(http_method: "post")
  #http_sender_options(http_method: "post", basic_auth_login: "prbb", basic_auth_password: "139865ba7c377")

  def perform(id, uri = "http://195.151.135.103/api/leads", *args, &block)
    a = send_request(id, uri, *args, &block)
    Rails.logger.debug "Response from SPL: #{a}"
  end

  def params_to_send(id, *args, &block)
    last_lead = Lead.find(id)
    {
      fio: last_lead.fio,
      phone: "+7#{last_lead.phone}",
      city: last_lead.city,
      region: last_lead.region,
      access_token: "ffdsqwe123asewq",
      traffic_source: last_lead.traffic_source,
      ad_company: last_lead.ad_company,
      partner_source: last_lead.partner_source,
      amount: last_lead.amount,
      product_id: last_lead.product_id,
      external_id: last_lead.external_id,
      external_ip: last_lead.external_ip,
      scoring_uid: id,
      description: "Заявка из Scoring"
    }
  end
end
