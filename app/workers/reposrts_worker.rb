# -*- encoding : utf-8 -*-

class ReportsWorker
  include Sidekiq::Worker
  sidekiq_options(queue: :default, retry: false, backtrace: true)

  attr_reader :lead_ids, :fields_for_uploading, :user_id, :partner_source, :first_day, :last_day, :client_ids

  def perform(conditions, lead_ids, fields_for_uploading, user_id, partner_source, first_day, last_day, client_ids, only_processed = false)
    path_args = get_path_args(conditions)
    conditions == "excel" ? export_to_excel(path_args[:file_path], lead_ids, fields_for_uploading, user_id, partner_source, first_day, last_day, client_ids, only_processed)
                          : export_to_csv(path_args[:file_path], lead_ids, fields_for_uploading, user_id, partner_source, first_day, last_day, client_ids, only_processed)
    build_report(path_args, user_id)

  end

  def self.on_period(first_day, last_day)
    where('created_at >= :first_day', first_day: first_day).where('created_at <= :last_day', last_day: last_day)
  end

  def self.find_group(lead_ids)
    where(
        'partner_source in (:client_ids)',
        client_ids: lead_ids
    )
  end

  private

  def get_path_args(conditions)
    file_name = "#{Time.current.to_s.split.join("-")}"[/(.*)\:\d\d/]
    conditions == "excel" ? (file_format = ".xlsx")
                          : (file_format = ".csv")
    file_path = Pathname.new("#{Rails.root}/public/reports/#{file_name + file_format}")
    file_url = "reports/#{file_name + file_format}"

    return {file_name: file_name, file_path: file_path, file_url: file_url}
  end

  def export_to_excel(file_path, lead_ids, fields_for_uploading, user_id, partner_source, first_day, last_day, client_ids, only_processed)
    @progress = Progress.find_or_create_by(user_id: user_id)
    p = Axlsx::Package.new
    p.workbook do |wb|
      message = 0.0
      @progress.update(value: "#{message}")
      style_shout = wb.styles.add_style sz: 16, b: true, alignment: { horizontal: :center }
      wb.add_worksheet(name: "Заявки") do |sheet|
        sheet.add_row add_top_header(partner_source, lead_ids)
        leads_array = get_leads(partner_source, first_day,last_day, client_ids, only_processed)
        size = leads_array.count
        leads_array.each_with_index do |lead, index|
          message = check_state_and_return_message(size, index, message)
          sheet.add_row add_row_to_sheet(fields_for_uploading, lead, lead_ids, partner_source)
        end
      end
      message = "done"
      @progress.update(value: "#{message}")
    end
    p.serialize file_path
  end


  def export_to_csv(file_path, lead_ids, fields_for_uploading, user_id, partner_source, first_day, last_day, client_ids, only_processed)
    @progress = Progress.find_or_create_by(user_id: user_id)
    message = 0.0
    @progress.update(value: "#{message}")
    leads_array = get_leads(partner_source, first_day,last_day, client_ids, only_processed)
    size = leads_array.count

    CSV.open(file_path, "wb") do |csv|
      csv << add_top_header(partner_source, lead_ids)
      leads_array.each_with_index do |lead, index|
        message = check_state_and_return_message(size, index, message)
        csv << add_row_to_sheet(fields_for_uploading, lead, lead_ids, partner_source)
      end # ...
    end
    message = "done"
    @progress.update(value: "#{message}")
  end

  def get_leads(partner_source, first_day,last_day, client_ids, only_processed)
    admin = (partner_source == "admin")
    if admin
      only_processed ? Lead.all.on_period(first_day, last_day).find_partners(client_ids, admin).where('spl_status IS NOT NULL').order(updated_at: :desc) # only processed apps
                     : Lead.all.on_period(first_day, last_day).find_partners(client_ids, admin).order(created_at: :desc) # all apps
    else
      Lead.all.on_period(first_day, last_day).find_partners(client_ids, admin).where('spl_status IS NOT NULL').order(updated_at: :desc) # only processed apps
    end
  end

  def add_top_header(partner_source, lead_ids)
    partner_source == "admin" ? lead_ids : lead_ids.map {|e| e =~ /телефон/i ? "Хэш телефона" : e }
  end

  def add_row_to_sheet(fields_for_uploading, lead, lead_ids, partner_source)
    special_fields = %w[Телефон]
    fields_for_uploading.map { |key, value|
      key.to_s != "Телефон" ? ( ( lead.send(value) || "N/A" ) if (lead_ids - special_fields).include?(key.to_s) )
                            : ( partner_source != "admin" ? ( Digest::SHA256.base64digest(lead.send(value).to_s).truncate(15) ) : ( (lead.send(value) || "N/A" ) if lead_ids.include?(key.to_s) ) )
    }.compact
  end

  def check_state_and_return_message(size, index, message)
    if ((index>1 && size>0) && (index % (10) ) == 0)   ### Каждые 10__+ записей посмотреть на сколько процентов выполнена работа и обновить модель
       message = index.to_f/(size+1)
       @progress.update(value: "#{message}")
    end
    return message
  end

  def build_report(path_args, user_id)
    Report.find_or_create_by(title: path_args[:file_name], user_id: user_id, url: path_args[:file_url])
  end

 end
