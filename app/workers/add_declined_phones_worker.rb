class AddDeclinedPhonesWorker
  include Sidekiq::Worker
  sidekiq_options(queue: :default, retry: false, backtrace: true)

  def perform(filename)
    import(filename)
  end

  private

  def import(file)
    Rails.logger.debug 'opening excel document'
    @excel = open_spreadsheet(file)
    Rails.logger.debug 'opened excel document'
    header = @excel.row(1)
    @excel.sheets.each_with_index do |sheet, index|
      if index != 0
        @excel.default_sheet = sheet
          2.upto(@excel.last_row) do |line|
            save_phone_if_not_exist(line)
          end
      end
    end
  end

  def save_phone_if_not_exist(line)
    hash = {}
    # p 'trying to save_phone'
    x = ('A'..'Z').to_enum
    x.each_with_index {|e, index| index % 2 == 0 ? hash[e] = x.next.next : x.next }
    hash.each_pair do |date_index, phone_index|
      date = @excel.cell(line, date_index) rescue ""
      phone = @excel.cell(line, phone_index).to_s.gsub(/(\(|\)\s)/,'') rescue ""

      unless (date == "" || phone == "")
        @phone = Phone.find_or_initialize_by(phone: phone.to_i)
        if @phone.new_record?
          @phone.updated_at = date
          @phone.save!
          Rails.logger.debug "saving phone #{phone}"
        end
      else
        Rails.logger.debug "not found valid phone in excel cell #{phone_index}"
      end
    end
  end

  def open_spreadsheet(file)
    case File.extname(file)
      when ".csv" then Roo::Csv.new("#{Rails.root}/public/phones/#{file}")
      when ".xlsx" then Roo::Excelx.new("#{Rails.root}/public/phones/#{file}")
      when ".xls" then Roo::Excel.new("#{Rails.root}/public/phones/#{file}")
      else
        raise "Unknown file type: #{file.original_filename}"
    end
  end

end