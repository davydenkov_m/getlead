class AddClientPhonesWorker
  include Sidekiq::Worker
  sidekiq_options(queue: :default, retry: false, backtrace: true)

  def perform(filename)
    import_csv(filename)
  end

  private

  def import_csv(file)
    mongo_log(begin: 'opening spreadsheet document')
    @spreadsheet = open_spreadsheet(file)
    mongo_log(end: 'opened spreadsheet document')

    header = @spreadsheet.row(1)

    @spreadsheet.sheets.each_with_index do |sheet, index|
        @spreadsheet.default_sheet = sheet
        2.upto(@spreadsheet.last_row) do |line|
          safe_phone_if_not_exist(line)
        end
      end
  end

  def safe_phone_if_not_exist(line)
    phones = @spreadsheet.cell(line, 'A').split(';').each {|e| e.gsub!(/\D/,'')} - [""] rescue nil
    if phones
      phones.map {|phone|
        @phone = ClientPhone.find_or_initialize_by(phone: phone.to_i)
        if @phone.new_record?
          @phone.updated_at = Time.now
          @phone.save! if phone.size == 10
          mongo_log phone: "saving phone #{phone} at #{Time.now}"
        else
          mongo_log phone: "phone_already exists"
        end
      }
    else
      mongo_log(message: "empty cell")
    end
  end

  def open_spreadsheet(file)
    case File.extname(file)
      when ".csv" then Roo::CSV.new("#{Rails.root}/public/client_phones/#{file}")
      when ".xlsx" then Roo::Excelx.new("#{Rails.root}/public/client_phones/#{file}")
      when ".xls" then Roo::Excel.new("#{Rails.root}/public/client_phones/#{file}")
      else
        raise "Unknown file type: #{file.original_filename}"
    end
  end

end