# -*- encoding : utf-8 -*-


before 'deploy', 'rvm1:install:rvm'
#after "unicorn:stop", "mysidekiq:force_delete"

set :application, 'getLead'
set :scm, "git"

set :repo_url, 'git@github.com:DavydenkovM/GetLead.git'
set :branch, "master"
set :deploy_to, "/home/deployer/apps/getLead"
set :deploy_via, :remote_cache
set :use_sudo, false


=begin
set :mongodb_logger_assets_dir, "public/assets" # where to put mongodb assets
after 'deploy:update_code', 'mongodb_logger:precompile'
=end

set :format, :pretty
set :log_level, :info

#set :linked_files, %w{config/database.yml config/mongodb_logger.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/reports}
set :linked_dirs, fetch(:linked_dirs) + %w{public/system}

#set :normalize_asset_timestamps, false

set :rvm1_ruby_version, "2.1.1"
#set :rvm_ruby_version, "2.0.0-p353"
set :keep_releases, 3
set :unicorn_env, "production"

set :unicorn_binary, "#{shared_path}/bin/unicorn"
set :unicorn_config, "#{current_path}/config/unicorn.rb"
set :unicorn_pid,    "#{shared_path}/tmp/pids/unicorn.pid"

namespace :deploy do
  desc 'Restart application'
  task :restart do
    invoke 'unicorn:restart'
  end

  desc 'Remove any sidekiq'
end


