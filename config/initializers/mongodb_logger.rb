require 'mongodb_logger/server' # required


# this secure you web interface by basic auth, but you can skip this, if you no need this
=begin
MongodbLogger::Server.use Rack::Auth::Basic do |username, password|
  [username, password] == ['admin', 'password']
end
=end


=begin
db = Rails.logger.mongo_adapter.connection
p db
#<Mongo::DB:0x007fdc7c65adc8 @name="monkey_logs_dev" ... >
collection = Rails.logger.mongo_adapter.collection
p collection
#<Mongo::Collection:0x007fdc7a4d12b0 @name="development_log" .. >
=end
