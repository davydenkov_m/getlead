# -*- encoding : utf-8 -*-
WillPaginate::ViewHelpers.pagination_options[:previous_label] = '<< Предыдущая'
WillPaginate::ViewHelpers.pagination_options[:next_label] = 'Следующая >>'

if defined?(WillPaginate)
  module WillPaginate
    module ActiveRecord
      module RelationMethods
        alias_method :per, :per_page
        alias_method :num_pages, :total_pages
      end
    end
  end
end

module ActiveRecord
  class Relation
    alias_method :total_count, :count
  end
end
