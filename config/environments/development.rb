# -*- encoding : utf-8 -*-
GetLead::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = false
  config.eager_load = true
  config.assets.debug = true
  config.serve_static_assets = true
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = true
  config.action_mailer.raise_delivery_errors = false
  #config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.cache_store = :dalli_store
 # config.log_formatter = ::Logger::Formatter.new
 # config.log_level = :log

  config.encoding = 'utf-8'

end
