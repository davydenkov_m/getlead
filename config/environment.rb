# -*- encoding : utf-8 -*-
# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application

# $faye_thread ||= Thread.new do
#     system("rackup faye.ru -s thin -E production -o scoring.banklife.org")
# end

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

def mongo_log(hash)
  Rails.logger.debug "#{hash}"
  Rails.logger.add_metadata(:additional_data => hash) if Rails.logger.respond_to?(:add_metadata)
end

def mongo_find(hash)
  db = Rails.logger.mongo_adapter.connection
  collection = Rails.logger.mongo_adapter.collection
  collection.find('additional_data' => hash)
end

# mongo_find(:err => "qqq").each do |q| p q.to_hash["additional_data"] end

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

GetLead::Application.initialize!
